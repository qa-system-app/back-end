from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from qa_system.services.dialog import Dialog


app = FastAPI()

origins = [
    'http://localhost:3000',
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get('/get_answer')
def get_answer(question: str = ''):
    dialog = Dialog()
    text, emphasis_start, emphasis_end = dialog.search_for_answer(question)
    params = {'text': text, 'emphasis_start': emphasis_start, 'emphasis_end': emphasis_end}
    print('answer:', params)
    return params


@app.get('/get_prompts')
def get_prompts(question_part: str = ''):
    dialog = Dialog()
    return dialog.give_prompts(question_part)
