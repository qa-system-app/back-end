from pathlib import Path


def get_project_root() -> Path:
    return Path(__file__).parent


def get_static_dir() -> Path:
    return get_project_root() / 'static'


def get_queries_dir() -> Path:
    return get_static_dir() / 'sparql_queries'


def get_wikidata_queries_dir() -> Path:
    return get_queries_dir() / 'wikidata'


def get_instances_dir() -> Path:
    return get_static_dir() / 'instances'


def get_wiki_terms_dir() -> Path:
    return get_static_dir() / 'wiki_terms'
