SELECT ?oLabel
WHERE
{
  wd:$$s schema:description ?oLabel .
  FILTER (LANG(?oLabel) = 'ru') .
}
