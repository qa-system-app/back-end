SELECT ?oLabel
WHERE
{
  ?s ?literalProp "$$l"@ru .
  ?s wdt:$$p ?o
  SERVICE wikibase:label { bd:serviceParam wikibase:language "ru,en". }
}
