from pymorphy3 import MorphAnalyzer
from pymorphy3.analyzer import Parse


class TextProcessor:
    def __init__(self, morph: MorphAnalyzer):
        self.morph = morph

    @staticmethod
    def inflect_dependent_words(noun: Parse, dependent_words: [Parse], case: str = 'nomn') -> [Parse]:
        inflected_words = []
        for word in dependent_words:
            if word.tag.POS not in ['ADJF', 'ADJS', 'PRTF', 'PRTS']:
                inflected_words.append(word)
                continue
            inflected_word = word.inflect({case, noun.tag.number})
            if noun.tag.gender:
                inflected_word = inflected_word.inflect({noun.tag.gender})
            inflected_words.append(inflected_word)
        return inflected_words

    def process_up_to_noun(self, words: [str], only_nomn: bool = False) -> (Parse, [Parse]):
        parsed_before_noun = []
        noun = None
        for word in words:
            parsed = self.morph.parse(word)[0]
            if ('NOUN' in parsed.tag) and (not only_nomn or parsed.tag.case in ['nomn', 'accs']):
                noun = parsed
                break
            parsed_before_noun.append(parsed)
        return noun, parsed_before_noun

    @staticmethod
    def produce_sentence(before_noun: [Parse], noun: Parse, after_noun: [str]) -> str:
        sentence = [parsed.word for parsed in before_noun]
        sentence.append(noun.word)
        sentence += after_noun
        return ' '.join(sentence)
