from pymorphy3 import MorphAnalyzer

from qa_system.services.wiki_entries import WikidataProperty
from qa_system.services.text_processing import TextProcessor


class AnswerSchema:
    def __init__(self, predicate: WikidataProperty, subj: [str], objs: [[str]], morph: MorphAnalyzer):
        self.predicate = predicate
        self.subj = subj
        self.objs = objs

        self.morph = morph
        self.text_processor = TextProcessor(morph)

    def decline(self, phrase: [str]) -> [str]:
        noun, before_noun = self.text_processor.process_up_to_noun(phrase)
        if not noun:
            return ' '.join(phrase)
        before_noun_inflected = self.text_processor.inflect_dependent_words(
            noun, before_noun, self.predicate.case
        )
        noun_inflected = noun.inflect({self.predicate.case})
        return self.text_processor.produce_sentence(
            before_noun_inflected, noun_inflected, phrase[len(before_noun) + 1:]
        )

    @staticmethod
    def prettify_nl_answer(nl_answer: [str]) -> str:
        return ' '.join(nl_answer).capitalize() + '.'

    @staticmethod
    def count_symbols(padding: int, phrase: [str]) -> int:
        length = padding
        for word in phrase:
            length += len(word)
        length += len(phrase) - 1  # to account whitespaces
        return length

    def process_emphasis(self, nl_answer: [str]) -> (int, int):
        emphasis_start = self.count_symbols(0, nl_answer) + 1
        emphasis = []
        for obj in self.objs:
            emphasis.append(self.decline(obj))
        emphasis_str = ', '.join(emphasis)
        emphasis_end = emphasis_start + len(emphasis_str)
        nl_answer.append(emphasis_str)
        return emphasis_start, emphasis_end


class AnswerSchemaVerb(AnswerSchema):
    def __init__(self, predicate: WikidataProperty, subj: [str], objs: [[str]], morph: MorphAnalyzer):
        self.descr = 'SPO'
        super().__init__(predicate, subj, objs, morph)

    def get_nl_answer(self) -> (str, int, int):
        nl_answer = []
        nl_answer += self.subj
        nl_answer.append(self.predicate.name)
        emphasis_start, emphasis_end = self.process_emphasis(nl_answer)
        return self.prettify_nl_answer(nl_answer), emphasis_start, emphasis_end


class AnswerSchemaOf(AnswerSchema):
    def __init__(self, predicate: WikidataProperty, subj: [str], objs: [str], morph: MorphAnalyzer):
        self.descr = 'S-PO'
        super().__init__(predicate, subj, objs, morph)

    def get_nl_answer(self) -> (str, int, int):
        nl_answer = []
        nl_answer += self.subj
        nl_answer.append('–')
        nl_answer.append(self.predicate.name)
        emphasis_start, emphasis_end = self.process_emphasis(nl_answer)
        return self.prettify_nl_answer(nl_answer), emphasis_start, emphasis_end


class AnswerSchemaIs(AnswerSchema):
    def __init__(self, predicate: [str], subj: [str], objs: [str], morph: MorphAnalyzer):
        self.descr = 'O-этоPS'
        super().__init__(predicate, subj, objs, morph)

    def get_nl_answer(self) -> (str, int, int):
        nl_answer = []

        emphasis_start = 0
        emphasis_str = ', '.join(' '.join(obj) for obj in self.objs)
        emphasis_end = emphasis_start + len(emphasis_str)

        nl_answer.append(emphasis_str)

        nl_answer += ['–', 'это']
        nl_answer.append(self.predicate.name)

        nl_answer.append(self.decline(self.subj))

        return self.prettify_nl_answer(nl_answer), emphasis_start, emphasis_end
