from typing import Type

from pymorphy3 import MorphAnalyzer

from qa_system.services.nl_answer.schemas import AnswerSchemaOf, AnswerSchemaVerb, AnswerSchemaIs
from qa_system.services.wiki_entries import WikidataProperty


class AnswerGenerator:
    def __init__(self, morph: MorphAnalyzer):
        self.morph = morph

    def make_answer(self, predicate: WikidataProperty, subj: str, objs: [str]) -> str:
        schema = self.choose_schema(predicate)
        objs_split = list(map(lambda phrase: phrase.split(), objs))
        full_answer = schema(predicate, subj.split(), objs_split, self.morph).get_nl_answer()
        return full_answer

    def choose_schema(self, predicate: WikidataProperty) -> (
            Type[AnswerSchemaOf] | Type[AnswerSchemaVerb] | Type[AnswerSchemaIs]
    ):
        predicate_name = predicate.name.split()
        if predicate_name[0] == 'это':
            return AnswerSchemaOf
        for word in predicate_name:
            parsed = self.morph.parse(word)[0]
            if 'VERB' in parsed.tag and '3per' in parsed.tag:
                return AnswerSchemaVerb
        return AnswerSchemaIs


def run():
    # Example
    morph = MorphAnalyzer()

    prop_data = ['P31', 'это частный случай', 'gent']
    subj = 'линал'
    objs = ['раздел математики']

    # prop_data = ['P2579', 'изучается в', 'loct']
    # subj = 'алгебраическая группа'
    # objs = ['групповой объект', 'алгебраическое многообразие']

    prop_data = ['P31', 'частный случай', 'gent']
    subj = 'алгебраическая группа'
    objs = ['групповой объект', 'алгебраическое многообразие']

    prop = WikidataProperty(*prop_data)
    ans, start, end = AnswerGenerator(morph).make_answer(prop, subj, objs)
    print('answer', ans)
    print(start, end)
    print('before', ans[:start])
    print('emphasis', ans[start:end])
    print('after', ans[end:])


if __name__ == '__main__':
    run()
