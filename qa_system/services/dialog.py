import random

import requests
from pymorphy3 import MorphAnalyzer

from qa_system.services.wiki_entries import get_wiki_entries
from qa_system.services.constants import WIKIDATA_URL
from qa_system.services.common import read_query_from_file
from qa_system.services.nl_answer.answer_generation import AnswerGenerator
from qa_system.services.focus_identification import FocusIdentifier


class QuestionTypeNotAllowed(Exception):
    def __init__(self):
        message = 'Недопустимый тип вопроса'
        super().__init__(message)


class Dialog:
    def __init__(
            self,
            rewrite_props: bool = False,
            update_items: bool = False,
            rewrite_items: bool = False,
    ):
        self.wiki_entries = get_wiki_entries(rewrite_props, update_items, rewrite_items)
        self.morph = MorphAnalyzer()
        self.answer_generator = AnswerGenerator(self.morph)
        self.focus_identifier = FocusIdentifier(self.morph)

    def identify_question_wording(self, question: str) -> str:
        question_wording = ''
        for label in self.wiki_entries.unique_prop_labels:
            if question.startswith(label):
                question_wording = label
                break
        if not question_wording:
            raise QuestionTypeNotAllowed
        return question_wording

    def gather_relevant_predicates(self, question_wording: str) -> [str]:
        predicates = []
        for prop in self.wiki_entries.props:
            if question_wording in prop.labels:
                predicates.append(prop.identifier)
        return predicates

    def identify_subject(self, string: str) -> str:
        subj = ''
        for item in self.wiki_entries.items:
            if string in item.labels:
                subj = item.identifier
        return subj

    @staticmethod
    def send_request(query: str) -> [str]:
        response = requests.get(WIKIDATA_URL, params={'format': 'json', 'query': query})
        data = response.json()
        rows = data['results']['bindings']
        if not rows:
            return ''
        answers = [row['oLabel']['value'] for row in rows]
        return answers

    @staticmethod
    def wiki_queries(predicates: [str], literal: str, subj: str = ''):
        for predicate in predicates:
            if predicate == 'description':
                if subj:
                    template_name = 'get_obj_with_descr_and_subj.txt'
                else:
                    template_name = 'get_obj_with_descr_and_literal.txt'
            elif predicate in ['P18', 'P6802', 'P5555']:  # image
                if subj:
                    template_name = 'get_obj_with_prop_and_subj_no_label.txt'
                else:
                    template_name = 'get_obj_with_prop_and_literal_no_label.txt'
            else:
                if subj:
                    template_name = 'get_obj_with_prop_and_subj.txt'
                else:
                    template_name = 'get_obj_with_prop_and_literal.txt'
            query_template = read_query_from_file(template_name)
            query = (
                query_template
                .replace("$$s", subj)
                .replace("$$p", predicate)
                .replace("$$l", literal)
            )
            yield query, predicate

    def search_for_obj_in_ontology(self, predicates: [str], literal: str, subj: str = ''):
        objs = []
        predicate_used = ''
        print('Possible queries:')
        for query, predicate in self.wiki_queries(predicates, literal, subj):
            print(query)
            objs = self.send_request(query)
            if objs:
                predicate_used = predicate
                break
        return objs, predicate_used

    def get_ontology_triple(self, raw_question: str) -> (str, str, str):
        question = raw_question.strip().rstrip('?').rstrip().lower()
        print('question:', question)
        question_wording = self.identify_question_wording(question)
        question_focus = self.focus_identifier.get_question_focus(question, question_wording)
        predicates = self.gather_relevant_predicates(question_wording)
        subject = self.identify_subject(question_focus)
        onto_answers, predicate_used = self.search_for_obj_in_ontology(
            predicates,
            question_focus,
            subject
        )
        return predicate_used, question_focus, onto_answers

    def search_for_answer(self, raw_question: str) -> (str, int, int):
        predicate_used, subject, onto_answers = self.get_ontology_triple(raw_question)
        if onto_answers:
            prop = self.wiki_entries.get_prop_by_id(predicate_used)
            full_answer = self.answer_generator.make_answer(prop, subject, onto_answers)
        else:
            full_answer = ("Ответ не найден", 0, 0)
        return full_answer

    def give_prompts(self, raw_input: str):
        string = raw_input.lower().strip()
        if not string:
            return []
        print('raw_input', raw_input)
        print('question part:', string)
        prompts = []
        for label in self.wiki_entries.unique_prop_labels:
            if label.startswith(string):
                prompts.append(label + ' ' + random.choice(self.wiki_entries.items).labels[0])
        if not prompts:
            try:
                question_part = self.identify_question_wording(string)
                focus_substring = string.removeprefix(question_part).lstrip()
                for item in self.wiki_entries.items:
                    for label in item.labels:
                        if label.startswith(focus_substring):
                            prompts.append(question_part + ' ' + label)
            except QuestionTypeNotAllowed:
                pass
        print('prompts', prompts)
        return prompts


def run():
    # dialog = Dialog(False, True, False)  # Update items
    # dialog = Dialog(True, False, False)  # Update props
    dialog = Dialog()

    # user_input = "Как найти характеристический многочлен?"
    # user_input = "где изучается алгебраическая группа?"
    # user_input = "изображение математической функции"
    # user_input = "строка на тех для пустого множества"
    # user_input = "Что такое скаляр?"
    # user_input = 'изображение бинарного оператора'
    user_input = 'частным случаем чего является тривиальная группа'
    answer = dialog.search_for_answer(user_input)
    print(answer)

    # to check prompts for user:

    # for i in range(len(user_input)):
    #     print('-------')
    #     to_check = user_input[:i]
    #     print(to_check)
    #     print(dialog.give_prompts(to_check))


if __name__ == '__main__':
    run()
