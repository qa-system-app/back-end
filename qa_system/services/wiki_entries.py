import csv
from time import sleep
from itertools import islice

import requests

from qa_system.utils import get_wiki_terms_dir, get_instances_dir
from qa_system.services.common import read_query_from_file, save_instance, load_instance
from qa_system.services.constants import PLACEHOLDER, WIKIDATA_URL


ENTRIES_INSTANCE_FILENAME = 'wiki_entries.pickle'


class TooManyRequestsError(Exception):
    def __init__(self):
        super().__init__()


class WikidataEntry:
    def __init__(self, identifier: str, labels: [str] = None):
        self.identifier = identifier
        if not labels:
            self.labels = []
        else:
            self.labels = labels

    def __repr__(self):
        if self.labels:
            return f"{self.identifier:}: {self.labels[0]}"
        return f"{self.identifier}"


class WikidataItem(WikidataEntry):
    def __init__(self, identifier: str, labels: [str] = None):
        super().__init__(identifier, labels)


class WikidataProperty(WikidataEntry):
    def __init__(self, identifier: str, name: str, case: str, labels: [str] = None):
        super().__init__(identifier, labels)
        self.name = name
        self.case = case


class WikidataEntries:
    def __init__(self):
        self.items: [WikidataItem] = []
        self.props: [WikidataProperty] = []
        self.unique_prop_labels: [str] = set()

    def init_items(self):
        self.items = []
        terms_path = get_wiki_terms_dir() / 'terms.csv'
        with terms_path.open('r', encoding='utf-8') as f:
            terms_reader = csv.reader(f)
            for row in terms_reader:
                self.items.append(WikidataItem(row[1]))

    @staticmethod
    def _add_labels_to_entry(query_template: str, entry: WikidataEntry):
        query = query_template.replace(PLACEHOLDER, entry.identifier)
        response = requests.get(WIKIDATA_URL, params={'format': 'json', 'query': query})
        print(response)
        if response.status_code == 429:
            raise TooManyRequestsError
        data = response.json()
        rows = data['results']['bindings']
        for row in rows:
            entry.labels.append(row['label']['value'])

    def add_labels_to_items(self, rewrite: bool = False):
        query_template = read_query_from_file("get_all_labels_of_entry.txt")

        synonyms_filepath = get_wiki_terms_dir() / 'term_labels.csv'
        num_processed_terms = sum(1 for _ in open(synonyms_filepath, 'rb'))

        with synonyms_filepath.open('a', newline='', encoding='utf-8') as f:
            if rewrite:
                f.truncate(0)
            writer = csv.writer(f)
            i = 0
            for entry in islice(self.items, num_processed_terms, None):
                item_done = False
                while not item_done:
                    try:
                        self._add_labels_to_entry(query_template, entry)
                        row = [entry.identifier] + entry.labels
                        writer.writerow(row)
                        i += 1
                        if i % 5 == 0:
                            minutes = 3
                            print(f'Sleep for {minutes} minutes...')
                            sleep(minutes * 60)
                        item_done = True
                    except TooManyRequestsError:
                        minutes = 5
                        print(f'Sleep for {minutes} minutes...')
                        sleep(minutes * 60)

    def fill_props(self):
        self.props = []
        filepath = get_wiki_terms_dir() / "question_types.csv"
        with filepath.open('r', encoding='utf-8') as f:
            props_reader = csv.reader(f)
            for row in props_reader:
                identifier = row[0]
                name = row[2]
                case = row[3]
                labels = row[4:]
                self.props.append(WikidataProperty(identifier, name, case, labels))

    def save_item_labels(self):
        filepath = get_wiki_terms_dir() / 'term_labels.csv'
        with filepath.open('w', newline='', encoding='utf-8') as f:
            writer = csv.writer(f)
            for item in self.items:
                row = [item.identifier] + item.labels
                writer.writerow(row)

    def add_item_labels_from_file(self):
        filepath = get_wiki_terms_dir() / 'term_labels.csv'
        with filepath.open('r', newline='', encoding='utf-8') as f:
            reader = csv.reader(f)
            for i, row in enumerate(reader):
                self.items[i].labels = row[1:]

    def gather_unique_prop_labels(self):
        self.unique_prop_labels.clear()
        for prop in self.props:
            for label in prop.labels:
                if label:
                    self.unique_prop_labels.add(label)

    def get_prop_by_id(self, identifier: str) -> WikidataProperty:
        for prop in self.props:
            if prop.identifier == identifier:
                return prop

    def process_props(self):
        self.fill_props()
        self.gather_unique_prop_labels()

    def process_items(self, rewrite_items: bool = False):
        self.init_items()
        self.add_labels_to_items(rewrite_items)


def get_wiki_entries(rewrite_props: bool = False, update_items: bool = False, rewrite_items: bool = False):
    if not (get_instances_dir() / ENTRIES_INSTANCE_FILENAME).exists():
        entries = WikidataEntries()
        entries.process_props()
        entries.process_items()
        save_instance(ENTRIES_INSTANCE_FILENAME, entries)
        print('Wiki entries were created and saved')
    else:
        entries = load_instance(ENTRIES_INSTANCE_FILENAME)
        print('Wiki entries were loaded')
    if rewrite_props:
        entries.process_props()
        save_instance(ENTRIES_INSTANCE_FILENAME, entries)
        print('Wiki props were rewritten')
    if rewrite_items or update_items:
        entries.process_items(rewrite_items)
        save_instance(ENTRIES_INSTANCE_FILENAME, entries)
        print('Wiki items were updated')

    print(entries.items)
    print(entries.props)

    return entries
