from pymorphy3 import MorphAnalyzer
from pymorphy3.analyzer import Parse

from qa_system.services.text_processing import TextProcessor


class FocusIdentifier:
    def __init__(self, morph: MorphAnalyzer):
        self.morph = morph
        self.text_processor = TextProcessor(morph)

    @staticmethod
    def normalize_noun(noun: Parse) -> Parse:
        noun_nominative = noun.inflect({'nomn'})
        noun_nominative_sing = noun_nominative.inflect({'sing'})
        normalized_noun = noun_nominative_sing if noun_nominative_sing else noun_nominative
        return normalized_noun

    def get_question_focus(self, question: str, question_wording: str):
        raw_focus = question.removeprefix(question_wording).lstrip()
        raw_focus_words = raw_focus.split()
        noun, parsed_before_noun = self.text_processor.process_up_to_noun(raw_focus_words)
        if not noun:
            return raw_focus

        normalized_noun = self.normalize_noun(noun)
        inflected_words_before_noun = self.text_processor.inflect_dependent_words(
            normalized_noun, parsed_before_noun
        )
        return self.text_processor.produce_sentence(
            inflected_words_before_noun,
            normalized_noun,
            raw_focus_words[len(parsed_before_noun) + 1:]
        )
