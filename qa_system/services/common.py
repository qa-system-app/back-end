import pickle
from typing import Any

from qa_system.utils import get_project_root, get_instances_dir


def read_query_from_file(filename: str, ontology: str = 'wiki') -> str:
    ontology_folder = 'onto_math_pro' if ontology == 'math' else 'wikidata'
    filepath = get_project_root() / 'static' / 'sparql_queries' / ontology_folder / filename
    with filepath.open('r') as f:
        query = f.read()
    return query


def save_instance(filename: str, instance: Any) -> None:
    instance_dir = get_instances_dir()
    if not instance_dir.is_dir() or not instance_dir.exists():
        instance_dir.mkdir()
    filepath = instance_dir / filename
    with filepath.open('wb') as f:
        pickle.dump(instance, f)


def load_instance(filename: str) -> Any:
    filepath = get_instances_dir() / filename
    with filepath.open('rb') as f:
        instance = pickle.load(f)
    return instance
