import pytest

from qa_system.services.dialog import QuestionTypeNotAllowed


@pytest.mark.parametrize(
    "question,answer_ref",
    [
        ("Что такое скаляр?", "величина, определяемая одним числом"),
        ("Что такое обезьяна?", "группа приматов"),
        ("К чему относится базис?", "линейная алгебра"),
    ]
)
def test_ontology_obj_is_correct(dialog_init, question, answer_ref):
    triple = dialog_init.get_ontology_triple(question)
    assert triple[2][0] == answer_ref


def test_not_allowed_question_type(dialog_init):
    question = "Что написала Энн Райс?"
    with pytest.raises(QuestionTypeNotAllowed):
        dialog_init.search_for_answer(question)


def test_alt_labels_search(dialog_init):
    q_with_main_label = "Что такое скалярная величина?"
    q_with_alt_label = "Что такое скаляр?"
    assert (
        dialog_init.get_ontology_triple(q_with_main_label)[2]
        == dialog_init.get_ontology_triple(q_with_alt_label)[2]
    )


@pytest.mark.parametrize(
    "question",
    ['  Что такое скаляр?  ', 'что такое   скаляр', 'что такое СКАЛЯР']
)
def test_question_formatting(dialog_init, question):
    answer_ref = "величина, определяемая одним числом"
    answer = dialog_init.get_ontology_triple(question)
    assert answer[2][0] == answer_ref
