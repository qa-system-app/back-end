import pytest
from pymorphy3 import MorphAnalyzer

from qa_system.services.focus_identification import FocusIdentifier


@pytest.mark.parametrize(
    'question,question_wording,correct_focus',
    [
        (
            'определение ножниц',
            'определение',
            'ножницы',
         ),
        (
            'информация о векторном пространстве над полем',
            "информация о",
            "векторное пространство над полем",
        ),
    ]
)
def test_focus_identification(question, question_wording, correct_focus):
    morph = MorphAnalyzer()
    fi = FocusIdentifier(morph)
    assert fi.get_question_focus(question, question_wording) == correct_focus
