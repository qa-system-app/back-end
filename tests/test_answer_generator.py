import pytest
from pymorphy3 import MorphAnalyzer

from qa_system.services.nl_answer.answer_generation import AnswerGenerator
from qa_system.services.wiki_entries import WikidataProperty


@pytest.mark.parametrize(
    'prop_data,subj,obj,correct_answer',
    [
        (
            ['P8864', 'нейтральный элемент', 'gent'],
            "умножение матриц",
            ["единичная матрица"],
            'Единичная матрица – это нейтральный элемент умножения матриц.',
         ),
        (
            ['P1552', 'обладает свойством', 'gent'],
            "умножение матриц",
            ["ассоциативность"],
            'Умножение матриц обладает свойством ассоциативности.',
        ),
        (
            ['P31', 'это частный случай', 'gent'],
            "умножение матриц",
            ["бинарная операция"],
            'Умножение матриц – это частный случай бинарной операции.',
        ),
        (
            ['P1269', 'тематически относится к', 'datv'],
            'базис',
            ['линейная алгебра'],
            'Базис тематически относится к линейной алгебре.',
        )
    ]
)
def test_nl_answer_generation(prop_data, subj, obj, correct_answer):
    morph = MorphAnalyzer()
    prop = WikidataProperty(*prop_data)
    answer_generator = AnswerGenerator(morph)
    nl_answer, _, _ = answer_generator.make_answer(prop, subj, obj)
    assert nl_answer == correct_answer
